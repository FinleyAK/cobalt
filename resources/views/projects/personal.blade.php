@section('title', 'Home')
@section('content')
    @extends('layouts.base')

    <section class="hero">
        <div class="hero-head">@include ('layouts.nav')</div>
    </section>

    <div class="hero-body is-white is-center">

            <div class="container has-text-centered">
            <figure class="image is-128x128 is-inline-block">
          <img src="{{ asset('img/avatar.png') }}">
        </figure>
        <span style="font-family: autobahn,serif;">
            <p class="title is-1">
                Personal Projects
              </p>
        </span>
              <br />
              <p class="subtitle is-6">
                Hey there! You are currently looking at the personal projects area! The personal projects are meant to showcase SixSquares' art, as well as ChronosAeon's music. Check out what they are both capable of by clicking the links below!
              </p>
            </div>
            <br />

            <div class="card">
                    <div class="card-content">
                        <h1 class="title is-4">SixSquares' Art:</h1>
                            <div class="columns is-multiline">
                                    <div class="column is-one-quarter-desktop is-half-tablet">
                                      <div class="card">
                                          <div class="card-image">
                                              <figure class="image is-5by3">
                                                <img src="{{ asset('img/wumpus.png') }}" alt="">
                                              </figure>
                                              <div class="card-content is-overlay is-clipped">
                                                <span class="tag is-info">
                                                        Wumpus Odyssey
                                                </span>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="column is-one-quarter-desktop is-half-tablet">
                                            <div class="card">
                                                <div class="card-image">
                                                    <figure class="image is-5by3">
                                                      <img src="{{ asset('img/banner.jpg') }}" alt="">
                                                    </figure>
                                                    <div class="card-content is-overlay is-clipped">
                                                      <span class="tag is-info">
                                                              Cobalt Studios
                                                      </span>
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="column is-one-quarter-desktop is-half-tablet">
                                                <div class="card">
                                                    <div class="card-image">
                                                        <figure class="image is-5by3">
                                                          <img src="{{ asset('img/wumpus_flop.gif') }}" alt="">
                                                        </figure>
                                                        <div class="card-content is-overlay is-clipped">
                                                          <span class="tag is-info">
                                                                  A Happy Wumpus
                                                          </span>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>


                                              <div class="column is-one-quarter-desktop is-half-tablet">
                                                    <div class="card">
                                                        <div class="card-image">
                                                            <figure class="image is-5by3">
                                                              <img src="{{ asset('img/moonwave.png') }}" alt="">
                                                            </figure>
                                                            <div class="card-content is-overlay is-clipped">
                                                              <span class="tag is-info">
                                                                      Moonwave
                                                              </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                            </div>



                    </div>
                </div>
<br />

            <div class="card">
                    <div class="card-content">
                        <h1 class="title is-4">ChronosAeons Music:</h1>

                    </div>
            </div>




    </div>


    @endsection
