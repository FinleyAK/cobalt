@section('title', 'Home')
@section('content')
    @extends('layouts.base')

    <section class="hero">
        <div class="hero-head">@include ('layouts.nav')</div>
    </section>
            <div class="hero-body is-white is-center">

                        <div class="container has-text-centered">
                        <figure class="image is-128x128 is-inline-block">
                      <img class="is-rounded" src="{{ asset('img/logo.gif') }}">
                    </figure>

                    <br />

                    <font face="autobahn"> <p class="title is-1">
                        @auth
                            Welcome to Cobalt Studios, {{ Auth::user()->username }}!
                            @else
                            Welcome to Cobalt Studios
                            @endauth
                          </p></font>

                        </div>
<hr style="background-color: white;" />
                      <div class="columns">
                          <div class="column">
                                <a href="boosters">

                                <div class="card">
                                        <div class="card-image">
                                          <figure class="image is-4by3">
                                            <img src="{{ asset('img/banner.png') }}" alt="Wumpus">
                                          </figure>
                                        </div>
                                        <div class="card-content" style="background-color: white;">
                                          <div class="media">
                                            <div class="media-left">
                                              <figure class="image is-48x48">
                                                <img src="{{ asset('img/logo.gif') }}" alt="Placeholder image">
                                              </figure>
                                            </div>
                                            <div class="media-content">
                                              <p class="title is-4 has-text-black">We are so glad you're here!</p>
                                              @desktop
                                              <p class="subtitle is-6 has-text-black">To your right there are some pages you might want to check out!</p>
                                              @enddesktop
                                              @mobile
                                              <p class="subtitle is-6 has-text-black">Below you there are some pages you might want to check out!</p>
                                              @endmobile
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                </a>
                          </div>

                          <div class="column">
                                <a href="about">
                                <div class="card">
                                        <div class="card-image">
                                          <figure class="image is-4by3">
                                            <img src="{{ asset('img/wumpus_banner.png') }}" alt="Wumpus">
                                          </figure>
                                        </div>
                                        <div class="card-content" style="background-color: white;">
                                          <div class="media">
                                            <div class="media-left">
                                              <figure class="image is-48x48">
                                                <img src="{{ asset('img/logo.gif') }}" alt="Placeholder image">
                                              </figure>
                                            </div>
                                            <div class="media-content">
                                              <p class="title is-4 has-text-black">Boosters</p>
                                              <p class="subtitle is-6 has-text-black">Check out all of the marvelous people who wisely decided to boost our Discord server!
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                </a>

                          </div>
                          <div class="column">
                                <a href="boosters">

                                <div class="card">
                                        <div class="card-image">
                                          <figure class="image is-4by3">
                                            <img src="{{ asset('img/banner_wump.png') }}" alt="Wumpus">
                                          </figure>
                                        </div>
                                        <div class="card-content" style="background-color: white;">
                                          <div class="media">
                                            <div class="media-left">
                                              <figure class="image is-48x48">
                                                <img src="{{ asset('img/logo.gif') }}" alt="Placeholder image">
                                              </figure>
                                            </div>
                                            <div class="media-content">
                                              <p class="title is-4 has-text-black">About us</p>
                                              <p class="subtitle is-6 has-text-black">Check out who's part of this amazing company and what it's purposes and goals are.
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                </a>
                          </div>

                          </div>
                      </div>
            </div>
    </section>
@endsection
<style>
    .card{
        transition: transform .2s;
    }
    .card:hover {
  -ms-transform: scale(1.1); /* IE 9 */
  -webkit-transform: scale(1.1); /* Safari 3-8 */
  transform: scale(1.1);
}
</style
