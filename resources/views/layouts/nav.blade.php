<font size="4px;" face="autobahn">
    <nav class="navbar" style="background-color: #2b3384;" role="navigation" aria-label="main navigation">
       <div class="navbar-brand">
          <a class="navbar-item" href="{{ route('home') }}">
          <img src="{{ asset('img/header.webp') }}">
          </a>
          <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
             data-target="events">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          </a>
       </div>
       <div id="events" class="navbar-menu">
          <a class="navbar-item" href="/" style="background-color: transparent !important;" class="has-icon"> 🏠Home</a>
          <div class="navbar-item has-dropdown is-hoverable" style="background-color: transparent !important;">
             <div class="navbar-link" style="background-color: transparent !important;">
                <span class="icon"><i class="fas fa-arrow-down"></i></span>
                💻Projects
             </div>
             <div class="navbar-dropdown">
                <a class="navbar-item" style="background-color: transparent !important;"  href="{{ route('project_personal') }}">
                   <div class="navbar-content">
                      <p>Personal Projects</p>
                   </div>
                </a>
                <a class="navbar-item" href="">
                   <div class="navbar-content">
                      <p>Games Projects</p>
                   </div>
                </a>
                <a class="navbar-item" href="">
                   <div class="navbar-content">
                      <p>Emoji Projects</p>
                   </div>
                </a>
                <a class="navbar-item" href="">
                   <div class="navbar-content">
                      <p>Animation Projects</p>
                   </div>
                </a>
             </div>
          </div>
          <div class="navbar-item has-dropdown is-hoverable" style="background-color: transparent !important;">
             <div class="navbar-link" style="background-color: transparent !important;">
                <span class="icon"><i class="fas fa-arrow-down"></i></span>
               <span>📝Commissions</span>
             </div>
             <div class="navbar-dropdown">
                <a style="background-color: transparent !important;"  class="navbar-item" href="">
                   <div class="navbar-content">
                      <p>Personal Projects</p>
                   </div>
                </a>
             </div>
          </div>
          <a class="navbar-item" style="background-color: transparent !important;"  href="{{ route('merch') }}">👕Merch</a>
          <a class="navbar-item" style="background-color: transparent !important;"  href="/">ℹ️Updates</a>
          <div class="navbar-end">
             @auth
             <div class="navbar-item has-dropdown is-hoverable" style="background-color: transparent !important;" >
                <div class="navbar-link has-icon" style="background-color: transparent !important;">
                   <span class="icon"><i class="fab fa-discord fa-fw"></i></span>
                   <span>{{ Auth::user()->username }}</span>
                </div>
                <div class="navbar-dropdown" style="background-color: transparent !important;" >
                   <a class="navbar-item" style="background-color: transparent !important;"  href="{{ route('logout') }}">
                      <div class="navbar-content">
                         <p>Logout</p>
                      </div>
                   </a>
                </div>
             </div>
             @else
             <a class="navbar-item" href="{{ route('login') }}" style="background-color: transparent !important;">Login with <img src="{{ asset('img/discord_logo.png') }}" height="28" alt=""></a>
             @endauth
          </div>
       </div>
    </nav>
 </font>
 @section ('js')
 <script type="text/javascript">
    $(function () {
        $('.navbar-burger').click(function () {
            $(".navbar-burger").toggleClass("is-active")
            $(".navbar-menu").toggleClass("is-active")
        })
    })
 </script>
 @endsection
 <style>
    .navbar-item:hover{
    color: grey !important;
    }
    .navbar-link:hover{
    color: grey !important;
    }
    .select:after{
    color: white;
    }
 </style>
