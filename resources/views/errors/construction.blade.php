@section('title', 'Under Construction')
@section('content')
    @extends('layouts.base')

    <section class="hero is-medium">
        <div class="hero-head">@include ('layouts.nav')</div>
        <div class="container">
            <h2 class="subtitle is-6">Sorry, but this page is</h2>
            <h1 class="title is-1">Under Construction</h1>
        </div>
    </section>

@endsection

