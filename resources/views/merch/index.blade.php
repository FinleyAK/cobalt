@section('title', 'Merch')
@section('content')
    @extends('layouts.base')

    <section class="hero">
        <div class="hero-head">@include ('layouts.nav')</div>
    </section>

    <div class="hero-body is-white is-center">

        <div class="container has-text-centered">
            <figure class="image is-128x128 is-inline-block">
                <img src="{{ asset('img/shirt.png') }}">
            </figure>
            <span style="font-family: autobahn,serif;">
            <p class="title is-1">
                Merch
              </p>
        </span>
            <br />
            <p class="subtitle is-6">
                Henlo! This is the Merch page! Interested in buying some of our merch! Well click on one of the fancy buttons to buy em! We have sticker, posters of different sizes, mugs, phone cases, hoodies, t-shirts, pillows and much more!
            </p>
        </div>

    </div>
@endsection

